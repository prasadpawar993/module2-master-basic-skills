**_Object detection_** is a computer vision technique that works to identify and locate objects within an image or video. Specifically, bject detection draws bounding boxes around these detected objects, which allow us to locate where said objects are in (or how they move through) a given scene
Object detection is commonly confused with image recognition, so before we proceed, it’s important that we clarify the distinctions between them.
![fig](https://www.fritz.ai/images/object_detection_vs_image_recognition.jpg)

Why is object detection important?
Object detection is inextricably linked to other similar computer vision techniques like image recognition and image segmentation, in that it helps us understand and analyze scenes in images or video.
But there are important differences. Image recognition only outputs a class label for an identified object, and image segmentation creates a pixel-level understanding of a scene’s elements. What separates object detection from these other tasks is its unique ability to locate objects within an image or video. This then allows us to count and then track those objects.
we can see how it can be applied in a number of ways:
Crowd counting
Self-driving cars
Video surveillance
Face detection
Anomaly detection

![Architecture](http://res.cloudinary.com/dyd911kmh/image/upload/f_auto,q_auto:best/v1522766478/0_k7ld-_ZCL6QOonRw_dsi41m.png)