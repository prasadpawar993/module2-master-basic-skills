**_Architecture of CNN_**.
![Architecture](https://missinglink.ai/wp-content/uploads/2019/08/LeNet-5-1998.png)

There are four layered concepts we should understand in Convolutional Neural Networks:
Convolution,
Pooling, 
Flattening,
Full Connected  (Fully Connected Layer).


1.**_Convolution Layer_**
    We have 4 steps for convolution:
    1.Line up the feature and the image
    2.Multiply each image pixel by corresponding feature pixel
    3.Add the values and find the sum
    4.Divide the sum by the total number of pixels in the feature
![Convolution layer](![alt tag](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/70_blog_image_3.png)
    
we add a activation fuction called relu . it activates the pixel which are greater than 0 and makes the pixel 0 which are less than 0
![Relu](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/71_blog_image_1.png) 


2 **_Pooling Layer_**
    In this layer we shrink the image stack into a smaller size. Pooling is done after passing through the activation layer.       We     do   this by implementing the following 4 steps:
    1.Pick a window size (usually 2 or 3)
    2.Pick a stride (usually 2)
    3.Walk your window across your filtered images
    4.From each window, take the maximum value

![Pooling layer](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/72_blog_image_3.png)

3.**_Flattening layer_**
     Flattening is converting the data into 1-dimensional array for inputting it to the next layer
![Flattening layer](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/73_blog_image_2.png)

4.**_Fully connected layer_**
    we have three layers in the full connection step:
    1.Input layer
    2.Fully-connected layer
    3.Output layer
![Full connected](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/74_blog_image_1.png)


















